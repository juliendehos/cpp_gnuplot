// RUNCPP_BUILD $(pkg-config --cflags --libs opencv) -lboost_iostreams -lboost_system 

#include "gnuplot-iostream.h"
#include <opencv2/opencv.hpp>
#include <vector>

void sendCvCol(Gnuplot & g, const cv::Mat m) {
    std::vector<float> v;
    m.col(0).copyTo(v);
	g.send1d(v);
}

int main() { 

    const std::string filename = "douarnenez.png";

    // load input image
    cv::Mat imgInput = cv::imread(filename);
    if (imgInput.channels()!=3 or imgInput.depth()!=CV_8U) {
        std::cerr << "error: only 3-channel 8-bits images are supported\n";
        exit(-1);
    }

    // split channels
    std::vector<cv::Mat> bgrPlanes;
    cv::split( imgInput, bgrPlanes );

    // compute histograms
    int histSize = 256;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    cv::Mat bHist, gHist, rHist;
    cv::calcHist( &bgrPlanes[0], 1, 0, cv::Mat(), bHist, 1, &histSize, &histRange);
    cv::calcHist( &bgrPlanes[1], 1, 0, cv::Mat(), gHist, 1, &histSize, &histRange);
    cv::calcHist( &bgrPlanes[2], 1, 0, cv::Mat(), rHist, 1, &histSize, &histRange);

    // plot histograms
	Gnuplot gp;
    gp << "set style data lines \n";
    gp << "set xrange [0:255] \n";
    gp << "set grid xtics ytics \n";
	gp << "plot '-' lc rgb 'blue' notitle, \
        '-' lc rgb 'green' notitle, \
        '-' lc rgb 'red' notitle \n";
    sendCvCol(gp, bHist);
    sendCvCol(gp, gHist);
    sendCvCol(gp, rHist);

    return 0;
}


