# cpp_gnuplot

[![Build status](https://gitlab.com/juliendehos/cpp_gnuplot/badges/master/build.svg)](https://gitlab.com/juliendehos/cpp_gnuplot/pipelines) 

Test C++ gnuplot interfaces :
- [gnuplot-iostream](https://github.com/dstahlke/gnuplot-iostream)
- [gnuplot-cpp](https://code.google.com/archive/p/gnuplot-cpp/)

Application: plot RGB histograms computed with OpenCV.

![](douarnenez.png)

![](screenshot.png)

