CXXFLAGS = -std=c++11 -Wall -Wextra $(shell pkg-config --cflags opencv) 
LIBS = $(shell pkg-config --libs opencv) -lboost_iostreams -lboost_system 

all:
	$(CXX) $(CXXFLAGS) -o test_gnuplot-iostream.out test_gnuplot-iostream.cpp $(LIBS)
	$(CXX) $(CXXFLAGS) -o test_gnuplot_i.out test_gnuplot_i.cpp $(LIBS)

clean:
	rm test_gnuplot_i.out test_gnuplot-iostream.out

