with import <nixpkgs> {}; 

stdenv.mkDerivation {
    name = "cppgnuplot";
    buildInputs = [ stdenv pkgconfig opencv boost ];
    src = ./.;
    installPhase = "mkdir -p $out/bin ; cp *.out $out/bin";
}

