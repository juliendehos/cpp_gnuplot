// RUNCPP_BUILD $(pkg-config --cflags --libs opencv) 

#include "gnuplot_i.hpp"

#include <opencv2/opencv.hpp>
#include <vector>

class GpMat : public cv::Mat {

    public:
        size_t size() const {
            return rows*cols;
        }

        float operator[](int i) const {
            return at<float>(i);
        }

};

void waitCin() {
    std::cin.clear();
    std::cin.ignore(std::cin.rdbuf()->in_avail());
    std::cin.get();
}

void waitCv() {
    while (true) {
        if (cv::waitKey(100) == 27)
            break;
    }
}

int main() { 

    const std::string filename = "douarnenez.png";

    // load input image
    cv::Mat imgInput = cv::imread(filename);
    if (imgInput.channels()!=3 or imgInput.depth()!=CV_8U) {
        std::cerr << "error: only 3-channel 8-bits images are supported\n";
        exit(-1);
    }

    // split channels
    std::vector<cv::Mat> bgrPlanes;
    cv::split( imgInput, bgrPlanes );

    // compute histograms
    int histSize = 256;
    float range[] = { 0, 256 };
    const float* histRange = { range };
    GpMat bHist, gHist, rHist;
    cv::calcHist( &bgrPlanes[0], 1, 0, cv::Mat(), bHist, 1, &histSize, &histRange);
    cv::calcHist( &bgrPlanes[1], 1, 0, cv::Mat(), gHist, 1, &histSize, &histRange);
    cv::calcHist( &bgrPlanes[2], 1, 0, cv::Mat(), rHist, 1, &histSize, &histRange);

    // plot histograms
    Gnuplot g2;
    g2.set_xrange(0, 255).set_grid();
    g2.set_style("lines linecolor rgb 'blue' ").plot_x(bHist, "");
    g2.set_style("lines linecolor rgb 'green' ").plot_x(gHist, "");
    g2.set_style("lines linecolor rgb 'red' ").plot_x(rHist, "");

    waitCin();

    return 0;
}

